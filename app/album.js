const path = require("path");
const express = require("express");
const multer = require("multer");
const { nanoid } = require("nanoid");
const config = require("../config");
const Album = require("../models/Album");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({ storage });

const router = express.Router();

router.get("/", async (req, res) => {
    try {
        const albums = await Album.find().populate("artist", "title");
        return res.send(albums);

    } catch (e) {
        res.sendStatus(500);
    }
});

router.get("/:id", async (req, res) => {
    try {
        const album = await Album.findOne({ _id: req.params.id }).populate("artist", "title description");
        return res.send(album);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", upload.single("image"), async (req, res) => {
    const albumInfo = req.body;

    try {
        if (req.file) {
            albumInfo.image = req.file.filename;
        }

        const album = new Album(albumInfo);
        await album.save();
        res.send(album);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;
