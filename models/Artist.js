const mongoose = require("mongoose");

const artistSchema = new mongoose.Schema ({
    title: {
        type: String,
        required: true,
    },
    image: {
        type: String,
    },
    description: {
        type: String,
    }
});

const Artist = mongoose.model('Artist', artistSchema);
module.exports = Artist;