const express = require("express");
const Track = require("../models/Track");

const router = express.Router();

router.get("/", async (req, res) => {
    try {
        const track = await Track.find().populate("album", "title", );
        return res.send(track);

    } catch (e) {
        res.sendStatus(500);
    }
});

router.post("/", async (req, res) => {
    const trackInfo = req.body;
    try {
        const track = new Track(trackInfo);
        await track.save();
        res.send(track);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;
