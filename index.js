const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");
const exitHook = require('async-exit-hook');
const album = require("./app/album");
const artist = require("./app/artist");
const track = require("./app/track");


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

app.use('/album', album);
app.use('/track', track);
app.use('/artist', artist);

const port = 8000;


const run = async () => {
    await mongoose.connect('mongodb://localhost/fm',
        {useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async (callback) => {
        await mongoose.disconnect();
        console.log('disconnect');
        callback();
    });
};

run().catch(console.error)



