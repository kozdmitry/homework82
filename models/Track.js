const mongoose = require ("mongoose");

const truckSchema = new mongoose.Schema ({
    title: {
        type: String,
        required: true,
    },
    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Album",
        required: true,
    },
    timing: {
        type: String
    },
});

const Track = mongoose.model('Track', truckSchema);
module.exports = Track;
